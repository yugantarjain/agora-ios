These are the UI mockups for the Agora iOS app. 

The mock-ups include all the mentioned screens/functions with an additional screen (Vote) where users can see the elections to which they are invited to vote and vote too.  
However, the mock-ups for Ballot and Result screens are not included since it was said to ignore them on Gitter. If they are required then please let me know and I'll update as soon as possible.

@Thuva4 @designkenal @prudhvir3ddy please have a look at these mock-ups and let me know your thoughts, I'm looking forward to your feedback and suggestions. Thanks!

## Splash Screen

| Light | Dark |
| ------ | ------ |
| ![Screen_Shot_2020-03-23_at_5.55.13_PM](/uploads/e3221df28c5efadb61cbb1e9efc7388a/Screen_Shot_2020-03-23_at_5.55.13_PM.png) | ![Screen_Shot_2020-03-23_at_5.57.01_PM](/uploads/9032b002f5f4ed90d55ed931c2237324/Screen_Shot_2020-03-23_at_5.57.01_PM.png) |

## Authentication
The authentication related screens have been designed to be easily dismissible and shown only when the user needs it instead of strictly at the launch of an app; this is in line with the best practices for user experience.

| Light | Dark |
| ------ | ------ |
| ![Screen_Shot_2020-03-23_at_5.56.28_PM](/uploads/55f2b514b7a374d5ef03d58ad9568aeb/Screen_Shot_2020-03-23_at_5.56.28_PM.png) | ![Screen_Shot_2020-03-23_at_5.59.21_PM](/uploads/3a63ba86a80f599fcaad9e2aece2b08c/Screen_Shot_2020-03-23_at_5.59.21_PM.png) |
| ![Screen_Shot_2020-03-23_at_6.01.11_PM](/uploads/8a36df24938427ded018f8782d7efd9a/Screen_Shot_2020-03-23_at_6.01.11_PM.png) | ![Screen_Shot_2020-03-23_at_6.00.58_PM](/uploads/efaf2ee67f5ab6bff94a0cc8760bd1ea/Screen_Shot_2020-03-23_at_6.00.58_PM.png) | 
| ![Screen_Shot_2020-03-23_at_5.56.40_PM](/uploads/c4d1e0bbd05c82505f98af9b97f07693/Screen_Shot_2020-03-23_at_5.56.40_PM.png) | ![Screen_Shot_2020-03-23_at_6.02.29_PM](/uploads/e82827a62eb800032e4ff5bc888a1700/Screen_Shot_2020-03-23_at_6.02.29_PM.png) |
| ![Screen_Shot_2020-03-23_at_6.04.43_PM](/uploads/3807ee9624f99fa0a4976e577bb6b5a8/Screen_Shot_2020-03-23_at_6.04.43_PM.png) | ![Screen_Shot_2020-03-23_at_6.04.26_PM](/uploads/835e538134f2c05cb59125123b8a96cd/Screen_Shot_2020-03-23_at_6.04.26_PM.png) |

## Dashboard
| Light | Dark |
| ------ | ------ |
| ![Screen_Shot_2020-03-23_at_6.10.33_PM](/uploads/5c8f79beb681c741efc8ddc029271553/Screen_Shot_2020-03-23_at_6.10.33_PM.png) | ![Screen_Shot_2020-03-23_at_6.11.53_PM](/uploads/87febfd1fd75ff988fc24a6a020515c0/Screen_Shot_2020-03-23_at_6.11.53_PM.png) |

## Create Election & Add Candidates
| Light | Dark |
| ------ | ------ |
| ![Screen_Shot_2020-03-23_at_6.10.40_PM](/uploads/b25652b8d6be2e04825db7a7fe8785ca/Screen_Shot_2020-03-23_at_6.10.40_PM.png) | ![Screen_Shot_2020-03-23_at_6.11.55_PM](/uploads/9e902b79747f24f7b286ebe7f7522b41/Screen_Shot_2020-03-23_at_6.11.55_PM.png) |
| ![Screen_Shot_2020-03-23_at_6.10.47_PM](/uploads/f128fa8a81e0ad49b729e399a483d27e/Screen_Shot_2020-03-23_at_6.10.47_PM.png) | ![Screen_Shot_2020-03-23_at_6.11.58_PM](/uploads/8fc54b112da1a3053a0209e364c021b6/Screen_Shot_2020-03-23_at_6.11.58_PM.png) |

## Edit and Delete election
| Light | Dark |
| ------ | ------ |
| ![Screen_Shot_2020-03-23_at_8.01.40_PM](/uploads/8421f4bdbd8f943e205a15d2f51c874c/Screen_Shot_2020-03-23_at_8.01.40_PM.png) | ![Screen_Shot_2020-03-23_at_8.01.22_PM](/uploads/33087c97b8c828cf495e653087f0e070/Screen_Shot_2020-03-23_at_8.01.22_PM.png) |

## Add Voters
| Light | Dark |
| ------ | ------ |
| ![Screen_Shot_2020-03-23_at_6.13.13_PM](/uploads/61e8910994bf634ad36f1b64e4c5f109/Screen_Shot_2020-03-23_at_6.13.13_PM.png) | ![Screen_Shot_2020-03-23_at_6.12.58_PM](/uploads/1436ea6e7ea5e9fd9f238f5fc8a497a3/Screen_Shot_2020-03-23_at_6.12.58_PM.png) |

## Vote Screen
Users can get an overview of all the elections they have been invited to as a voter and vote in required elections.

| Light | Dark |
| ------ | ------ |
| ![Screen_Shot_2020-03-23_at_6.11.19_PM](/uploads/41c0b74f5a3f6d5592d983781bf9cdb8/Screen_Shot_2020-03-23_at_6.11.19_PM.png) | ![Screen_Shot_2020-03-23_at_6.12.06_PM](/uploads/8335aeda3a12065918933c41e5cb84aa/Screen_Shot_2020-03-23_at_6.12.06_PM.png) |